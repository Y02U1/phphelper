import subprocess
import json

from flask import Flask, render_template, request

application = Flask(__name__)


def build_template(json_file):
    with open(json_file) as json_data:
        d = json.load(json_data)
        d = sorted(d, key=lambda k: k['title'])
        total = ""
        for func in d:
            try:
                total = total + render_template("help.html",
                                                title=func['title'],
                                                synopsis=func['synopsis'],
                                                desc=func['desc'],
                                                params=func['params'],
                                                returnval=func['returnval'],
                                                errors=func['errors']
                                                )
            except KeyError:
                total = total + render_template("help.html",
                                                title=func['title'],
                                                desc=func['desc'],
                                                )
        return total


@application.route('/')
def root():
    return render_template("root.html")


@application.route('/generate', methods=['POST'])
def generate():
    JSON_FILE = "funcs.json"
    FUNC_FILE = "foreign.txt"
    data = request.form['functions'].split("\r\n")
    with open(FUNC_FILE, "w+") as foreign:
        foreign.write("\n".join(data))
    with open(JSON_FILE, "w+") as jsonfile:
        process = subprocess.Popen(
            ['scrapy', 'crawl', "phpnet", "-a", f"function={FUNC_FILE}", "-t", "json", "-o", "-"],
            stdout=jsonfile)
        process.wait()
    content = build_template(JSON_FILE)

    return render_template("final.html", content=content)


if __name__ == "__main__":
    application.run(debug=True)
