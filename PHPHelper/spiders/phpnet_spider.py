import scrapy


class PhpnetSpider(scrapy.Spider):
    name = "phpnet"

    def __init__(self, function='', **kwargs):
        self.start_urls = []
        # Read file
        with open(function, "r") as functions_file:
            for line in functions_file.readlines():
                statement_encoded = line.rstrip().replace("_", "-")
                self.start_urls.append(f'https://secure.php.net/manual/en/function.{statement_encoded}.php')

        # Parent constructor
        super().__init__(**kwargs)

    def parse(self, response):
        title = response.css("h1.refname::text").extract_first()
        if title is not None:
            dt = response.css("div.parameters dl > dt > code::text").extract()
            dd = response.css("div.parameters dl > dd").extract()
            length = len(dt)
            params = {}
            for i in range(length):
                params[dt[i]] = dd[i]
            yield {
                'title': title,
                'synopsis': response.css("div.methodsynopsis").extract(),
                'desc': "".join(response.css("div.description > p").extract()),
                'params': params,
                'returnval': response.css("div.returnvalues > p.para").extract(),
                'errors': response.css("div.errors > p.para").extract()
            }
        else:
            yield {
                'title': response.css("h2.title::text").extract_first(),
                'desc': "".join(response.css("div.sect1 p.para").extract()),
            }